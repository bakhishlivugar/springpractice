package az.bakhishli.springdemoannotation;

public interface Coach {
    String getDailyWorkout();
    String getDailyFortunes();
}
