package az.bakhishli.springdemoannotation;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component("myCrazyFortuneService")
public class CrazyFortuneService implements FortuneService {
    private final String[] data = {
            "Random Crazy fortune 1",
            "Random Crazy fortune 2",
            "Random Crazy fortune 3"
    };


    @Override
    public String getDailyFortune() {
        return data[new Random().nextInt(data.length)];
    }
}
