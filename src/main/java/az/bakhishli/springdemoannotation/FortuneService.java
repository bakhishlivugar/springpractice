package az.bakhishli.springdemoannotation;

import org.springframework.stereotype.Component;

public interface FortuneService {
    String getDailyFortune();
}
