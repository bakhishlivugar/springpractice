package az.bakhishli.springdemoannotation;

import org.springframework.beans.factory.annotation.Autowired;

public class KickboxingCoach implements Coach {


    private FortuneService fortuneService;

    @Autowired
    KickboxingCoach(FortuneService fortuneService){
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Hard work with your elbows";
    }

    @Override
    public String getDailyFortunes() {
        return fortuneService.getDailyFortune();
    }
}
