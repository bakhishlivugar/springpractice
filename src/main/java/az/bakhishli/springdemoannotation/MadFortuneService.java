package az.bakhishli.springdemoannotation;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class MadFortuneService implements FortuneService {

    private static final String FILE_NAME = "src/main/resources/fortunes.txt";
    private final static List<String> fortuneList = new ArrayList<>();


    @Override
    public String getDailyFortune() {
        return getRandomFortunes();
    }

    private static List<String> fillArrayFromFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
                fortuneList.add(strCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return fortuneList;
    }

    @PostConstruct
    private static String getRandomFortunes() {
        System.out.println("PostConstruct method in the action...");
        List<String> fortunes = fillArrayFromFile();
        Random random = new Random();
        int i = random.nextInt(fortunes.size());
        return fortunes.get(i);
    }

}
