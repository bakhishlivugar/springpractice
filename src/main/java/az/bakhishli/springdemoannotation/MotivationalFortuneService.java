package az.bakhishli.springdemoannotation;

import org.springframework.stereotype.Component;

@Component
public class MotivationalFortuneService implements FortuneService {

    @Override
    public String getDailyFortune() {
        return "Push yourself because no one will do it for you";
    }
}
