package az.bakhishli.springdemoannotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class NewAnnotationDemoApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(NewConfig.class);
        Coach kickBoxingCoach = annotationConfigApplicationContext.getBean("kickBoxingCoach", Coach.class);

        System.out.println(kickBoxingCoach.getDailyFortunes());
        System.out.println(kickBoxingCoach.getDailyWorkout());

        annotationConfigApplicationContext.close();
    }
}
