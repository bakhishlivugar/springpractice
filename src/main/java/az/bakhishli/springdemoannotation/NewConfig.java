package az.bakhishli.springdemoannotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("az.bakhishli.springdemoannotation")
public class NewConfig {

    @Bean
    public FortuneService motivationalFortuneService(){
        return new MotivationalFortuneService();
    }

    @Bean
    public Coach kickBoxingCoach(){
        return new KickboxingCoach(motivationalFortuneService());
    }
}
