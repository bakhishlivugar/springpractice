package az.bakhishli.springdemoannotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope("singleton") //default bean scope
public class TennisCoach implements Coach {

//    @Autowired
//    @Qualifier("myCrazyFortuneService")
    private FortuneService fortuneService;


    @Autowired
    public TennisCoach(@Qualifier("madFortuneService") FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }


    @Override
    public String getDailyWorkout() {
        return "Practice your backhand volley!";
    }

    @Override
    public String getDailyFortunes() {
        return fortuneService.getDailyFortune();
    }


    // Comment out for now...
//    @PostConstruct
//    private void doMyStartUp(){
//        System.out.println("Post construct method in the action");
//    }
//
//    @PreDestroy
//    private void doMyCleanUp(){
//        System.out.println("Pre-destroy method in the action");
//    }

}
